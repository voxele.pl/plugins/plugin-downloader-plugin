package pl.voxele.minecraft.plugin.downloader.repository;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class MavenRepositoryClient
{
    private final Gson gson = new GsonBuilder().create();

    public String get(MavenRepository repository, String path) throws IOException
    {
        try (InputStream inputStream = this.getStream(repository, path))
        {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes);

            return new String(bytes, StandardCharsets.UTF_8);
        }
    }

    public InputStream getStream(MavenRepository repository, String path) throws IOException
    {
        HttpURLConnection connection = (HttpURLConnection) new URL(repository.getUrl() + path).openConnection();
        connection.setRequestMethod("GET");

        if (repository.getUsername() != null && repository.getPassword() != null)
        {
            String token = Base64.getEncoder()
                    .encodeToString((repository.getUsername() + ":" + repository.getPassword()).getBytes(
                            StandardCharsets.UTF_8));
            connection.setRequestProperty("Authorization", "Basic " + token);
        }

        return connection.getInputStream();
    }
}
