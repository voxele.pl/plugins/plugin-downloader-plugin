package pl.voxele.minecraft.plugin.downloader.util;

import java.io.File;
import java.io.FilenameFilter;

public class PluginFilter implements FilenameFilter
{
    @Override
    public boolean accept(File dir, String name)
    {
        return name.endsWith(".jar");
    }
}
