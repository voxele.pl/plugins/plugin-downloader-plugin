package pl.voxele.minecraft.plugin.downloader;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.InvalidDescriptionException;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import pl.voxele.minecraft.plugin.downloader.plugin.PluginConfiguration;
import pl.voxele.minecraft.plugin.downloader.plugin.PluginInformation;
import pl.voxele.minecraft.plugin.downloader.repository.MavenRepository;
import pl.voxele.minecraft.plugin.downloader.repository.MavenRepositoryClient;
import pl.voxele.minecraft.plugin.downloader.util.PluginFilter;
import pl.voxele.minecraft.plugin.downloader.util.PluginUtil;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;

public class PluginDownloaderPlugin
        extends JavaPlugin
{
    private final Map<String, PluginInformation>   pluginsCache           = new HashMap<>();
    private final Map<String, MavenRepository>     repositoryMap          = new HashMap<>();
    private final Map<String, PluginConfiguration> pluginConfigurationMap = new HashMap<>();

    private final File pluginsDirectory = this.getFile().getParentFile();

    private final MavenRepositoryClient repositoryClient = new MavenRepositoryClient();

    @Override
    public void onLoad()
    {
        this.loadConfiguration();
        this.loadPluginCache();

        AtomicInteger count = new AtomicInteger();
        this.pluginConfigurationMap.forEach((pluginName, pluginConfiguration) ->
        {
            MavenRepository repository = this.repositoryMap.get(
                    pluginConfiguration.getRepository().toLowerCase(Locale.ROOT));
            if (repository == null)
            {
                return;
            }
            String basePath = "/" + pluginConfiguration.getGroup()
                    .replace('.', '/') + "/" + pluginConfiguration.getArtifact();

            this.getLogger().info("Looking for version of '" + pluginName + "' plugin");
            try
            {
                String latest = this.repositoryClient.get(repository, basePath + "/latest");
                this.getLogger().info("Latest version of plugin is '" + latest + "'");

                String jarName = pluginConfiguration.getArtifact() + "-" + latest + ".jar";
                String jarPath = basePath + "/" + latest + "/" + jarName;
                String md5 = this.repositoryClient.get(repository, jarPath + ".md5");

                PluginInformation information = this.pluginsCache.get(pluginName);
                if (information != null && PluginUtil.md5(information.getFile()).equals(md5))
                {
                    this.getLogger().info("MD5 checksum of plugin is the same as local plugin's checksum");
                    return;
                }
                this.getLogger()
                        .info("Local version of plugin is different than in maven repository, start downloading...");

                File tempFile = new File(this.getDataFolder(),
                        pluginConfiguration.getGroup() + "." + pluginConfiguration.getArtifact());
                if (!tempFile.getParentFile().exists())
                {
                    tempFile.getParentFile().mkdirs();
                }
                try (ReadableByteChannel readableByteChannel = Channels.newChannel(
                        this.repositoryClient.getStream(repository, jarPath)))
                {
                    try (FileChannel fileChannel = new FileOutputStream(tempFile).getChannel())
                    {
                        fileChannel.transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                    }
                }
                this.getLogger().info("New version of plugin downloaded successfully");
                PluginDescriptionFile description = this.getPluginLoader().getPluginDescription(tempFile);
                if (!pluginName.equals(description.getName()))
                {
                    this.getLogger()
                            .warning(
                                    "Name of new version of plugin is different than in configuration (expected: " + pluginName + ", found: " + description
                                            .getName() + ")");
                    return;
                }

                File newFile = new File(this.pluginsDirectory, jarName);
                if (!tempFile.renameTo(newFile))
                {
                    this.getLogger().warning("Can not move new version of plugin to plugins directory :'(");
                    return;
                }
                if (information != null && !newFile.equals(information.getFile()) && !information.getFile().delete())
                {
                    this.getLogger().warning("Can not delete old version of plugin from plugins directory ;-;");
                    return;
                }
                this.getLogger().info("New version of plugin successfully installed <3");
                count.incrementAndGet();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
            catch (InvalidDescriptionException e)
            {
                this.getLogger().log(Level.WARNING, "Can not find plugin.yml in new version of plugin, aborted!", e);
            }
        });

        if (count.get() > 0)
        {
            this.getLogger().info("Found new versions of plugins, try to reload!");

            try
            {
                Field descriptionField = JavaPlugin.class.getDeclaredField("logger");
                descriptionField.setAccessible(true);

                for (Plugin plugin : Bukkit.getPluginManager().getPlugins())
                {
                    descriptionField.set(plugin, null);
                }
            }
            catch (NoSuchFieldException | IllegalAccessException e)
            {
                e.printStackTrace();
            }
            Bukkit.getPluginManager().clearPlugins();

            try
            {
                Method loadPluginsMethod = Bukkit.getServer().getClass().getDeclaredMethod("loadPlugins");
                loadPluginsMethod.invoke(Bukkit.getServer());
            }
            catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e)
            {
                e.printStackTrace();
            }
        }
    }

    private void loadConfiguration()
    {
        this.saveConfig();
        this.reloadConfig();

        FileConfiguration configuration = this.getConfig();

        ConfigurationSection repositoriesSection = configuration.getConfigurationSection("repositories");
        if (repositoriesSection == null)
        {
            configuration.set("repositories", new HashMap<>());
        }
        else
        {
            repositoriesSection.getKeys(false).forEach(repositoryName ->
            {
                ConfigurationSection repositorySection = repositoriesSection.getConfigurationSection(repositoryName);
                if (repositorySection == null)
                {
                    return;
                }
                MavenRepository repository = new MavenRepository();
                repository.setUrl(repositorySection.getString("url"));
                repository.setUsername(repositorySection.getString("credentials.username"));
                repository.setPassword(repositorySection.getString("credentials.password"));

                this.repositoryMap.put(repositoryName.toLowerCase(Locale.ROOT), repository);
            });
        }
        ConfigurationSection pluginsSection = configuration.getConfigurationSection("plugins");
        if (pluginsSection == null)
        {
            configuration.set("plugins", new HashMap<>());
        }
        else
        {
            pluginsSection.getKeys(false).forEach(pluginName ->
            {
                ConfigurationSection pluginSection = pluginsSection.getConfigurationSection(pluginName);
                if (pluginSection == null)
                {
                    return;
                }
                PluginConfiguration pluginConfiguration = new PluginConfiguration();
                pluginConfiguration.setGroup(pluginSection.getString("group"));
                pluginConfiguration.setArtifact(pluginSection.getString("artifact"));
                pluginConfiguration.setVersion(pluginSection.getString("version"));
                pluginConfiguration.setRepository(pluginSection.getString("repository"));

                if (!this.repositoryMap.containsKey(pluginConfiguration.getRepository().toLowerCase(Locale.ROOT)))
                {
                    this.getLogger().warning("Can not find repository '" + pluginConfiguration.getRepository() + "'");
                }

                this.pluginConfigurationMap.put(pluginName, pluginConfiguration);
            });
        }
        this.saveConfig();
    }

    private void loadPluginCache()
    {
        File[] pluginsFiles = this.pluginsDirectory.listFiles(new PluginFilter());
        if (pluginsFiles == null)
        {
            return;
        }
        for (File pluginFile : pluginsFiles)
        {
            try
            {
                PluginDescriptionFile description = this.getPluginLoader().getPluginDescription(pluginFile);
                PluginInformation information = new PluginInformation(pluginFile);
                information.setVersion(description.getVersion());

                this.pluginsCache.put(description.getName(), information);
            }
            catch (InvalidDescriptionException e)
            {
                e.printStackTrace();
            }
        }
    }
}
