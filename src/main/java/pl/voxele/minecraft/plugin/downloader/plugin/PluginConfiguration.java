package pl.voxele.minecraft.plugin.downloader.plugin;

public class PluginConfiguration
{
    private String group;
    private String artifact;
    private String version;
    private String repository;

    public String getGroup()
    {
        return group;
    }

    public void setGroup(String group)
    {
        this.group = group;
    }

    public String getArtifact()
    {
        return artifact;
    }

    public void setArtifact(String artifact)
    {
        this.artifact = artifact;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }

    public String getRepository()
    {
        return repository;
    }

    public void setRepository(String repository)
    {
        this.repository = repository;
    }
}
