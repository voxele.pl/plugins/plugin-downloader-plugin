package pl.voxele.minecraft.plugin.downloader.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PluginUtil
{
    private static final char[] HEX_ARRAY = "0123456789abcdef".toCharArray();

    public static String md5(File file)
    {
        try
        {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(Files.readAllBytes(file.toPath()));

            byte[] digest = md.digest();
            return bytesToHex(digest);
        }
        catch (NoSuchAlgorithmException | IOException ignored)
        {
        }
        return "";
    }

    private static String bytesToHex(byte[] bytes)
    {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++)
        {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }
}
