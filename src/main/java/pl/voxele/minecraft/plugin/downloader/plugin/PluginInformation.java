package pl.voxele.minecraft.plugin.downloader.plugin;

import java.io.File;

public class PluginInformation
{
    private final File   file;
    private       String version;

    public PluginInformation(File file)
    {
        this.file = file;
    }

    public File getFile()
    {
        return file;
    }

    public String getVersion()
    {
        return version;
    }

    public void setVersion(String version)
    {
        this.version = version;
    }
}
